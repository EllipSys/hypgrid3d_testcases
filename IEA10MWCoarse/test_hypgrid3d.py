
import shutil
import os
import subprocess
import unittest
import numpy as np

E2D_PATH = os.getenv('ELLIPSYS2D_PATH')
h3d_bin = os.environ.get('h3d_bin', 'hypgrid3d')


def readX2D(filename, nd=2, include_ghost=True, mglevel=0):
    """
    read a mesh file (X2D) created with basis2d

    parameters
    ----------
    filename: str
        name of file to read
    name: str
        name given to the block
    nd: int
        dimension of grid, 2 or 3
    include_ghost: bool
        flag for including the ghost cells in the grid

    returns
    -------
    domain: object
        PGL.main.domain.Domain object
    """

    try:
        from scipy.io import FortranFile
    except:
        raise ImportError('Install scipy: pip install scipy')

    f = FortranFile(filename)

    # read number of blocks
    bsizes = np.zeros(5)
    bsize = bsizes[0] = f.read_ints(dtype=np.int32)[0]
    ni = bsize + 3
    nb = f.read_ints(dtype=np.int32)[0]
    dimInt = f.read_ints(dtype=np.int32)[0]
    print('dimInt', dimInt)
    mglev = 1
    for level in range(1,6):
        if np.mod(bsizes[level-1],2) == 0 and bsizes[level-1] > 2:
            bsizes[level] = bsizes[level-1] // 2
            mglev += 1
    print('Grid info: ni = %i, nblock = %i, mglev = %i\n' % (bsize, nb, mglev))
    # create a domain object
    attrv = []
    xv = []
    yv = []
    zv = []
    for level in range(mglev):
        attrv.append([f.read_ints(dtype=np.int32) for i in range(nb)])
        xv.append([f.read_reals() for i in range(nb)])
        yv.append([f.read_reals() for i in range(nb)])
        if nd == 3:
            zv.append([f.read_reals().T for i in range(nb)])

    if include_ghost:
        attr = np.zeros((ni, ni, nb), dtype=int)
        grid = np.zeros((ni, ni, nb, 3))
        for n in range(nb):
            grid[:, :, n, 0] = np.array(xv[mglevel][n]).reshape(ni, ni)
            grid[:, :, n, 1] = np.array(yv[mglevel][n]).reshape(ni, ni)
            if nd == 3:
                grid[:, :, n, 2] = np.array(zv[mglevel][n]).reshape(ni, ni)
            else:
                grid[:, :, n, 2] = np.zeros((ni, ni, 1))

    else:
        attr = np.zeros((ni-2, ni-2, nb), dtype=int)
        grid = np.zeros((ni-2, ni-2, nb, 3))
        for n in range(nb):
            bname = name + str(n)
            grid[:, :, n, 0] = np.array(xv[mglevel][n]).reshape(ni, ni)[1:-1,1:-1]
            grid[:, :, n, 1] = np.array(yv[mglevel][n]).reshape(ni, ni)[1:-1,1:-1]
            if nd == 3:
                grid[:, :, n, 2] = np.array(zv[mglevel][n]).reshape(ni, ni)[1:-1,1:-1]
            else:
                grid[:, :, n, 2] = np.zeros((bsize+1,bsize+1, 1))

    f.close()

    return attr, grid




def read_x3dunf(filename):
    """  
    read a mesh file (x3dunf) created with HypGrid3D 

    parameters
    ----------
    filename: str
        name of file to read

    returns
    -------
    grid: numpy.ndarray
        shape(ni, ni, ni, nblock, 3)
    attr: numpy.ndarray
        shape(ni, ni, ni, nblock)
    """

    try: 
        from scipy.io import FortranFile
    except:
        raise ImportError('Install scipy: pip install scipy')

    f = FortranFile(filename)

    # read number of blocks
    bsize = f.read_ints(dtype=np.int32)[0]
    ni = bsize + 3
    nb = f.read_ints(dtype=np.int32)[0]

    print('Block size', bsize, ni, nb)

    attr = [f.read_ints(dtype=np.int32) for i in range(nb)]
    x = [f.read_reals() for i in range(nb)]
    y = [f.read_reals() for i in range(nb)]
    z = [f.read_reals() for i in range(nb)]

    attrb = np.zeros((ni, ni, ni, nb), dtype=int)
    gridb = np.zeros((ni, ni, ni, nb, 3))

    for n in range(nb):
        a = np.array(attr[n]).reshape(ni, ni, ni).T
        xx = np.array(x[n]).reshape(ni, ni, ni).T
        yy = np.array(y[n]).reshape(ni, ni, ni).T
        zz = np.array(z[n]).reshape(ni, ni, ni).T
        attrb[:, :, :, n] = a
        gridb[:, :, :, n, 0] = xx
        gridb[:, :, :, n, 1] = yy
        gridb[:, :, :, n, 2] = zz

    f.close()

    return gridb, attrb



class TestHypGrid3D(unittest.TestCase):

    def test_hypgrid(self):

        shutil.copy('iea10mw_coarse.x2d', 'grid.x2d')
        command = "printf '%s\ny\n' | %s" % ('grid', os.path.join(E2D_PATH, 'basis2d'))
        subprocess.call(command, shell=True)

        attr2d, grid2d = readX2D('grid.X2D', nd=3)
        attr2dr, grid2dr = readX2D('iea10mw_coarseb.X2D', nd=3)
        diff = np.sum(np.abs(grid2d)) - np.sum(np.abs(grid2dr))
        self.assertAlmostEqual(diff, 0.e0, 5)

        command = "printf '%i\n2\n1\n' | %s" % (200, os.path.join(E2D_PATH, h3d_bin))
        subprocess.call(command, shell=True)

        # read reference
        gridr, attrr = read_x3dunf('iea10mw_coarse.x3dunf')
        # read computed grid 
        grid, attr = read_x3dunf('grid.x3dunf')

        diff = np.sum(np.abs(grid)) - np.sum(np.abs(gridr))
        print('2D Sums, ref: %20.16e, test value: %20.16e, diff: %20.16e' % (np.sum(np.abs(grid2dr)),
                                                                             np.sum(np.abs(grid2d)), diff))
        print('Sums, ref: %20.16e, test value: %20.16e, diff: %20.16e' % (np.sum(np.abs(gridr)), np.sum(np.abs(grid)), diff))

        self.assertAlmostEqual(diff, 0.e0, 5)


if __name__ == '__main__':

    unittest.main()
